// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { getRequestToken, login } from '@/axios-api/axiosApi';
import { LoginErrorType } from '@/types';
import { AxiosError } from 'axios';
import type { NextApiRequest, NextApiResponse } from 'next';
import Cookies from 'cookies';


export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if(req.method === 'POST'){
    try {
      const tokenResponse = await getRequestToken();
      const loginResponse = await login({
        username: req.body.username,
        password: req.body.password,
        request_token: tokenResponse.data.request_token,
      });
      const cookies = new Cookies(req, res)
      cookies.set('movie_db_token', loginResponse.data.request_token, {
        expires: new Date(loginResponse.data.expires_at),
        secure: process.env.NODE_ENV === "production",
        httpOnly: true,
        sameSite: true,
      })
      res.status(200).json(loginResponse.data);
    } catch(err) {
      const error = err as AxiosError<LoginErrorType>
      if(error.response?.data){
        res.status(error.response?.status).json(error.response.data);
      } else {
        res.status(error.response?.status || 500).json({
          success: false,
          status_message: 'Something went wrong, Try again later',
        });
      }
    }
  }
}