// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next';
import Cookies from 'cookies';


export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if(req.method === 'POST'){
    try {
      const cookies = new Cookies(req, res)
      cookies.set('movie_db_token', '', {
        expires: new Date(0),
        secure: process.env.NODE_ENV === "production",
        httpOnly: true,
        sameSite: true,
      })
      res.status(200).json({
        success: true,
        message: 'Logout successfull',
      });
    } catch(err) {
        res.status(400).json({
            success: false,
            message: 'Something went wrong, Try again later',
        });
    }
  }
}