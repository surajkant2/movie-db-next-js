import Head from 'next/head'
import styles from '@/styles/Home.module.css'
import { FormStateType, LoginErrorType, LoginReponseType } from '@/types';
import { useState } from 'react';
import axios, { AxiosError, AxiosResponse } from 'axios';
import { useRouter } from 'next/router';
import { toast } from 'react-toastify';
import ButtonLoader from '@/components/ButtonLoader';

export default function Home() {
  const router = useRouter();
  const [formState, setFormState] = useState<FormStateType>({
    username: {
      value: "",
      focusedOrNotEmpty: false,
    },
    password: {
      value: "",
      focusedOrNotEmpty: false,
    },
  });

  const [usernameError, setUsernameError] = useState<string>("");
  const [passwordError, setPasswordError] = useState<string>("");
  const [mainError, setMainError] = useState<string>("");
  const [loading, setLoading] = useState<boolean>(false);

  const handleSubmit:React.FormEventHandler<HTMLFormElement> = async (e) => {
    e.preventDefault();
    if (validate()) {
      setLoading(true);
      try {
        const loginResponse:AxiosResponse<LoginReponseType> = await axios.post('/api/login', {
          username: formState.username.value,
          password: formState.password.value,
        });
        if (loginResponse.data.success) {
          router.push('/movies')
          toast.success("Login successfull", {
            position: "bottom-right",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: false,
            draggable: true,
            progress: undefined,
            theme: "light",
          });
        }
        setLoading(false);
      } catch (err) {
        const error = err as AxiosError<LoginErrorType>;
        if(error?.response?.data?.status_code === 30){
          setMainError("Invalid credentials")
        } else {
          setMainError("Something went wrong. Login failed")
        }
        setLoading(false);
      }
    }
  };

  const handleChange = (e:React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setFormState({
      ...formState,
      [name]: {
        ...formState[name as keyof FormStateType],
        value: value,
      },
    });
  };

  const handleFocus = (e:React.FocusEvent<HTMLInputElement>) => {
    const { name } = e.target;
    setFormState({
      ...formState,
      [name]: {
        ...formState[name as keyof FormStateType],
        focusedOrNotEmpty: true,
      },
    });
  };

  const handleBlur = (e:React.FocusEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    if (!value) {
      setFormState({
        ...formState,
        [name]: {
          ...formState[name as keyof FormStateType],
          focusedOrNotEmpty: false,
        },
      });
    }
  };

  const validate = () => {
    let valid = true;
    setUsernameError("");
    setPasswordError("");
    setMainError("");

    if (!formState.username.value.trim()) {
      setUsernameError("Please enter your username");
      valid = false;
    }

    if (!formState.password.value.trim()) {
      setPasswordError("Please enter your password");
      valid = false;
    }

    return valid;
  };

  return (
    <>
      <Head>
        <title>Login | Insta Play</title>
      </Head>
      <main className={styles.main}>
        <div className="container">
          <div className="Login">
            <div className="login-box">
              <h1>Sign in</h1>
              <p className="instruction">Sign in to your Self Service Portal</p>
              <form onSubmit={handleSubmit}>
                <div className="input-and-error-wrap">
                  <div className="input-wrap">
                    <label
                      htmlFor="username"
                      className={`floating-placeholder ${
                        formState.username.focusedOrNotEmpty ? " floated" : ""
                      }`}
                    >
                      Username
                    </label>
                    <input
                      type="text"
                      id="username"
                      name="username"
                      onChange={handleChange}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      value={formState.username.value}
                    />
                  </div>
                  {usernameError && <p className="error-text">{usernameError}</p>}
                </div>

                <div className="input-and-error-wrap">
                  <div className="input-wrap">
                    <label
                      htmlFor="password"
                      className={`floating-placeholder ${
                        formState.password.focusedOrNotEmpty ? " floated" : ""
                      }`}
                    >
                      Password
                    </label>
                    <input
                      type="password"
                      id="password"
                      name="password"
                      onChange={handleChange}
                      onFocus={handleFocus}
                      onBlur={handleBlur}
                      value={formState.password.value}
                    />
                  </div>
                  {passwordError && <p className="error-text">{passwordError}</p>}
                </div>
                {mainError && <p className="error-text">{mainError}</p>}
                <button disabled={loading} type="submit" className="login-button">
                  <span>LOG IN</span>
                  {loading && <ButtonLoader />}
                </button>
              </form>
            </div>
          </div>
        </div>
      </main>
    </>
  )
}
