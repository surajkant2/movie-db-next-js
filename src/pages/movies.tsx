import { getMovies } from '@/axios-api/axiosApi';
import { MovieType } from '@/types';
import { GetServerSideProps } from 'next';
import Head from 'next/head';
import React from 'react';
import Image from '@/components/Image';
import MovieCard from '@/components/MovieCard';
import Pagination from '@/components/Pagination';
import { useRouter } from 'next/router';

interface MoviesPropsType {
  query: string;
  movies: MovieType[];
  totalResults: number;
  totalPages: number;
  page: number;
}

const Movies:React.FC<MoviesPropsType> = ({ movies, query, page, totalResults, totalPages }) => {
  const router = useRouter();

  const handlePageChange = (page: number) => {
    const queryObj:any = {};
    if(query){
      queryObj.query = query;
    }
    queryObj.page = page;
    router.push({query: queryObj})
  }

  return (
    <>
      <Head>
        <title>Movies | Insta Play</title>
      </Head>
      <div className="Movies">
        <Image className="banner" src="/banner.jpg" alt="Page Banner" />

        <div className="movies-wrapper">
          <div className="container">
            <h1>{query ? `Search results for: ${query}` : "Trending"}</h1>

            {totalResults === 0 ? (
              <p className="gen-text">No result found</p>
            ) : (<>
              <div className="row">
                {movies.map((movie) => (
                  <div className="col-3" key={movie.id}>
                    <MovieCard movie={movie} />
                  </div>
                ))}
              </div>

              <Pagination
                totalPages={totalPages}
                page={page}
                handlePageChange={handlePageChange}
              />
            </>)}
          </div>
        </div>
      </div>
    </>
  );
}

export default Movies

export const getServerSideProps:GetServerSideProps = async function ({query}) {
  const page = Number(query?.page) || 1;
  const searchQuery = query?.query as string || '';
  const response = await getMovies(page, searchQuery);

  return {
    props: {
      totalPages: response.data.total_pages,
      movies: response.data.results,
      totalResults: response.data.total_results,
      query: searchQuery,
      page: page,
    }
  }
}