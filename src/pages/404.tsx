import React from 'react';

const PageNotFound = () => {
  return (
    <div className="page-404">
        <div className="container">
            <h1>Page not found</h1>
            <p>Are you lost mate?</p>
        </div> 
    </div>
  )
};

export default PageNotFound;