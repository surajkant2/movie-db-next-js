import { getMovieDetails, getMovieTrailerTeaser } from '@/axios-api/axiosApi';
import Image from '@/components/Image';
import VideoModal from '@/components/VideoModal';
import { MovieDetailType } from '@/types';
import { imageUrl } from '@/util/util';
import { GetServerSideProps } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React, { useState } from 'react';


interface MovieDetailsPropsType {
  movieDetails: MovieDetailType;
  videoId: string;
}

const MovieDetails:React.FC<MovieDetailsPropsType> = ({movieDetails, videoId}) => {
  const router = useRouter();
  const [ showVideoModal, setShowVideoModal ] = useState<boolean>(false);

  const handleCloseVideoModal = () => setShowVideoModal(false);

  return (
    <>
      <Head>
        <title>{movieDetails.title + ' | Movies Details | Insta Play'} </title>
      </Head>
      <div className="MovieDetails">
        {(
          <div
            className="movie-details-master"
            style={{
              backgroundImage: `linear-gradient(90deg, #000 30%, #0000 100%)${
                movieDetails.backdrop_path
                  ? `, url(${imageUrl(movieDetails.backdrop_path)})`
                  : ""
              }`,
            }}
          >
            <div className="movie-backdrop-mobile-wrap">
              <Image
                src={
                  movieDetails.backdrop_path
                    ? imageUrl(movieDetails.backdrop_path)
                    : '/dummy-image.svg'
                }
                alt={movieDetails.title}
                className="movie-backdrop-mobile"
              />
              {videoId && (
                <button className='mobile-play-btn' onClick={() => setShowVideoModal(true)}>
                  <img src="/play-icon-big.svg" alt="play trailer" />
                </button>
              )}
              <button className="back-button" onClick={() => router.back()}>
                <img src="/arrow-left-icon.svg" alt="Go to previous page" />
              </button>
            </div>

            <div className="container">
              <div className="movie-details-wrap">
                <div className="flex f-aic">
                  <div className="left">
                    <button
                      className="back-button"
                      onClick={() => router.back()}
                    >
                      <img src="/arrow-left-icon.svg" alt="Go to previous page" />
                    </button>
                    <div className="details">
                      <h1>{movieDetails.title}</h1>
                      <p className="rating">
                        Rating:{" "}
                        {Math.trunc((movieDetails.vote_average / 2) * 10) / 10}
                        /5
                      </p>
                      <p className="summary">{movieDetails.overview}</p>
                    </div>
                    <table>
                      <tbody>
                        <tr>
                          <th scope="row">Release Date</th>
                          <td>{movieDetails.release_date}</td>
                        </tr>
                        <tr>
                          <th scope="row">Original Language</th>
                          <td>
                            {movieDetails.spoken_languages
                              .map((l) => l.english_name)
                              .join(", ")}
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div className="right">
                    {videoId && (
                      <button onClick={() => setShowVideoModal(true)}>
                        <img src="/play-icon-big.svg" alt="play trailer" />
                      </button>
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        {showVideoModal && (
          <VideoModal videoId={videoId} handleClose={handleCloseVideoModal} />
        )}
      </div>
    </>
  );
}

export default MovieDetails;

export const getServerSideProps:GetServerSideProps = async function ({query}) {
  const movieId:string = query?.movieId as string;
  const responseMovie = await getMovieDetails(movieId || '');
  const responseVideos = await getMovieTrailerTeaser(movieId || '');
  const videoId = responseVideos.data.results.find(
    (item) => item.type === "Trailer" || item.type === "Teaser"
  )?.key || "";


  return {
    props: {
      movieDetails: responseMovie.data,
      videoId: videoId,
    }
  }
}