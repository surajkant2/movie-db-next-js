import axios, { AxiosResponse } from "axios";
import { LoginDetailsType, LoginReponseType, LogoutReponseType, MovieDetailType, MoviesResponseType, VideosResponseType } from "../types";

const api = axios.create({
    baseURL: "https://api.themoviedb.org/3",
    params: {
        api_key: process.env.API_KEY,
    },
});

export const getMovies:((page:number, query:string)=>Promise<AxiosResponse<MoviesResponseType, any>>) = async (page, query) => {
    if (query) {
        return api.get<MoviesResponseType>("/search/movie", {
            params: {
                query: query,
                page: page,
            },
        });
    } else {
        return api.get<MoviesResponseType>("/trending/movie/week", {
            params: {
                page: page,
            },
        });
    }
};

export const getMovieDetails:((movieId:string)=>Promise<AxiosResponse<MovieDetailType, any>>) = (movieId) => {
    return api.get<MovieDetailType>(`/movie/${movieId}`);
};
  
export const getMovieTrailerTeaser:((movieId:string)=>Promise<AxiosResponse<VideosResponseType, any>>) = (movieId) => {
    return api.get<VideosResponseType>(`/movie/${movieId}/videos`);
};

export const getRequestToken:(()=>Promise<AxiosResponse<LoginReponseType, any>>) = () => {
    return api.get<LoginReponseType>(`/authentication/token/new`);
};

export const login:((loginDetails:LoginDetailsType) => Promise<AxiosResponse<LoginReponseType>>) = (loginDetails) => {
    return api.post<LoginReponseType>(`/authentication/token/validate_with_login`, loginDetails);
};