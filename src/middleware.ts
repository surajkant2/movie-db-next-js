import { NextResponse } from "next/server";

export function middleware(request:any){
    const accept = request.headers.get('accept');
    const pathname = request.nextUrl.pathname;
    if(accept === '*/*' || accept.includes('text/html')){
        if(pathname === '/' && request.cookies.get('movie_db_token')){
            return NextResponse.redirect(request.nextUrl.origin + '/movies');
        } else if(pathname !== '/' && !request.cookies.get('movie_db_token')){
            return NextResponse.redirect(request.nextUrl.origin + '/');
        }
    }
    return NextResponse.next();
} 

export const config = {
    matcher: [
      /*
       * Match all request paths except for the ones starting with:
       * - api (API routes)
       * - _next/static (static files)
       * - _next/image (image optimization files)
       * - favicon.ico (favicon file)
       */
      '/((?!api|_next/static|_next/image|favicon.ico).*)',
    ],
  }