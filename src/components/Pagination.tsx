interface PaginationPropsType {
    totalPages:number;
    page:number;
    handlePageChange:(page:number)=>void
}

const Pagination:React.FC<PaginationPropsType> = ({totalPages, page, handlePageChange}) => {
    if(totalPages === 1) {
        return null;
    }
    
    function getPagination(){
        const pageLinks = [];
        let from = 0;
        let to = 0;

        if (page < 3) {
            from = 1;
            to = totalPages > 5 ? 5 : totalPages;
        } else if (page > totalPages - 2) {
            from = totalPages - 4 < 1 ? 1 : totalPages - 4;
            to = totalPages;
        } else {
            from = page - 2 < 1 ? 1 : page - 2;
            to = page + 2 > totalPages ? totalPages : page + 2;
        }
    
        if(page !== 1){
            pageLinks.push(<button onClick={() => handlePageChange(page - 1)} className="pre pre-next" key="pre">
                <img src="/caret-left.svg" alt="Previous Page" />
            </button>);
        }
    
        if (page >= 5 && totalPages > 6) {
            pageLinks.push(
              <button onClick={() => handlePageChange(1)} key="1" className="num">
                1
              </button>
            );
            pageLinks.push(
              <span key="ellipsis-left" className="ellipsis">
                ...
              </span>
            );
        }
    
        for(let i = from; i <= to; i++){
            pageLinks.push(<button onClick={() => handlePageChange(i)} className={"num" + (page === i ? " active" : "")} key={i}>{i}</button>);
        }
    
        if (page <= totalPages - 4 && totalPages - 4 > 2) {
            pageLinks.push(
                <span key="ellipsis-right" className="ellipsis">
                  ...
                </span>
            );
            pageLinks.push(
              <button onClick={() => handlePageChange(totalPages)} key={totalPages} className="num">
                {totalPages}
              </button>
            );
        }
    
        if(page !== totalPages){
            pageLinks.push(<button onClick={() => handlePageChange(page + 1)} className="next pre-next" key="next">
                <img src="/caret-right.svg" alt="Next Page" />
            </button>);
        }

        return pageLinks;
    }

    return (
        <div className="Pagination">
            {getPagination()}
        </div>
    );
}
 
export default Pagination;