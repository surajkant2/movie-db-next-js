import Image from "./Image";
import Rating from "./Rating";
import { MovieType } from "../types";
import Link from "next/link";
import { imageUrl } from "@/util/util";

const MovieCard:React.FC<{movie:MovieType}> = ({movie}) => {
    return (
        <Link href={`/movie-details/${movie.id}`} className="MovieCard">
            <Image
                src={movie.backdrop_path ? imageUrl(movie.backdrop_path, 'w500') : '/dummy-image.svg'}
                alt={movie.title}
                ratio='500:281'
                className='movie-thumbnail'
            />
            <div className="movie-details">
                <div className="title-and-rating">
                    <h2 title={movie.title}>{movie.title}</h2>
                    <Rating rating={Math.round(movie.vote_average/2*10)/10} total={5} />
                </div>
                <img className="play-button" src="/play-icon.svg" alt="" />
            </div>
        </Link>
    );
}
 
export default MovieCard;