interface ImagePropsType {
  src:string;
  alt:string;
  ratio?:string;
  className:string;
}

const Image:React.FC<ImagePropsType> = ({ src, alt, ratio, className }) => {
    const getStyles = () => {
        const styles:React.CSSProperties = {
            width: "100%",
            position: "relative",
            height: 0,
        }
        if(ratio){
            styles.paddingBottom = (Number(ratio.split(":")[1]) / Number(ratio.split(":")[0])) * 100 + "%"
        }
        return styles;
    }

  return (
    <div
      className={className}
      style={getStyles()}
    >
      <img
        src={src}
        alt={alt}
        style={{
          height: "100%",
          width: "100%",
          position: "absolute",
          objectFit: "cover",
          objectPosition: "center",
        }}
      />
    </div>
  );
};

export default Image;
