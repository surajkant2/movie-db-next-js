import { LogoutReponseType } from "@/types";
import axios from "axios";
import { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { toast } from "react-toastify";
import ButtonLoader from "./ButtonLoader";

const Navbar:React.FC = () => {
  const [ loading, setLoading] = useState<boolean>(false);
  const [ loadingSearch, setLoadingSearch] = useState<boolean>(false);
  const [ formValue, setFormValue ] = useState<string>('');
  const [ formValueChanged, setFormValueChanged ] = useState<boolean>(false);
  const router = useRouter();
  const query = router.query.query;

  useEffect(() => {
    if(formValueChanged){
      setLoadingSearch(true)
      const id = setTimeout(() => {
        if(formValue.trim()){
          router.push({query: {
            query: formValue,
            page: 1,
          }})
        } else {
          router.push('/');
        }
        setFormValueChanged(false)
      }, 1000)
      return () => clearTimeout(id);
    } else if (typeof query === 'string') {
      setFormValue(query);
    }
  }, [formValue, query]);

  useEffect(() => {
    const setLoadingSearchFalse = () => setLoadingSearch(false)
    router.events.on('routeChangeComplete', setLoadingSearchFalse)

    return () => {
      router.events.off('routeChangeComplete', setLoadingSearchFalse)
    }
  }, [router])


  const logout = async () => {
    setLoading(true);
    try{
      setLoading(false);
      const response = await axios.post<LogoutReponseType>('/api/logout') 
      if(response.data.success){
        router.push('/');
        toast.success("Logout successfull", {
          position: "bottom-right",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: false,
          progress: undefined,
          theme: "light",
        });
      }
    } catch(err){
      toast.error('Logout failed', {
        position: "bottom-right",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
        theme: "light",
      });
      setLoading(false);
    }
  }

  const handleFormValueChange = (e:React.ChangeEvent<HTMLInputElement>) => {
    setFormValueChanged(true)
    setFormValue(e.target.value)
  }

  const isLoggedIn = () => router.pathname !== '/';

  return (
    <div className="Navbar">
      <div className="container">
        <div className="nav-wrap">
          <Link className="logo" href="/">
            <img src='/insta-play-logo.svg' alt="Insta Play" />
          </Link>

          {isLoggedIn() && (
            <>
              <div className="search-and-logout flex">
                {router.pathname === "/movies" && (
                  <form
                    className="search-form flex"
                    onSubmit={(e) => e.preventDefault()}
                  >
                    <input
                      type="text"
                      placeholder="Search movies"
                      id="searchbar"
                      value={formValue}
                      onChange={handleFormValueChange}
                    />
                    <label htmlFor="searchbar" className="search-icon">
                      <img src='/search-icon.svg' style={{opacity: loadingSearch ? 0 : 1}} alt="Search..." />
                      {loadingSearch &&
                        <ButtonLoader />
                      }
                    </label>
                  </form>
                )}

                <button
                  onClick={logout}
                  className="logout-button"
                  disabled={loading}
                >
                  <span style={{opacity: loading ? 0 : 1}}>
                    Logout
                  </span>
                  {loading && <ButtonLoader />}
                </button>
              </div>
            </>
          )}
        </div>
      </div>
    </div>
  );
};

export default Navbar;
