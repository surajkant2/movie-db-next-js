export const imageUrl:((path:string, size?:string)=>string) = (path, size = "original") => {
    return `https://image.tmdb.org/t/p/${size}${path}`;
};
  